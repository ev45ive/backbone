import _ from 'underscore'
import Backbone from 'backbone'
import UserModel from './user.model'

const Users = Backbone.Collection.extend({
    url: 'http://localhost:3000/users/',
    model: UserModel,
    initialize(){
        console.log('Userrs',this)
    }
});

export default Users;