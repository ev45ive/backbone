
import Users from './users/users.collection';
import UsersListView from './users/users-list.view';
//import {UserItemView} from './users/index'

function App({el}){
    this.el = el;
    this.init()
}
App.prototype = {
    init: function(){
        //console.log('Version:',this.version)

        // Models:
        this.users = new Users([
            {name:'User 1'},
            {name:'User 2'},
            {name:'User 3'}
        ])

        // Views:
        this.items = new UsersListView({
            el:this.el,
            collection: this.users
        });
        this.items.render()

        console.log("App", this)
    },
    version: '1.2.0'
}

export var secret = 123566;


export default App;