import 'bootstrap/dist/css/bootstrap.css'
import './style.css';

import _ from  'expose-loader?_!underscore'
import Backbone from 'expose-loader?Backbone!backbone'

import App from './app.js'

var app = new App({
    el: document.getElementById('app-root'),
})